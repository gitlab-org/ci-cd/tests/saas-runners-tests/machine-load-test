# Go Load Tester

This repository contains a simple Go program for basic performance and load testing. The program sleeps, calculates prime numbers using a CPU-intensive algorithm and allocates memory to create load and consume resources.

## Usage

1. Run the program with default settings:
   ```
   go run load-test.go
   ```

1. Run the program with custom settings using command line flags:

    ```
    go run load-test.go --bloat-mb 2048 --nth-prime 500000 --sleep 5
    ```

    - `--bloat-mb`: Memory usage in MB (default: 1024)
    - `--nth-prime`: Nth prime number to calculate (default: 1000000)
    - `--sleep`: Number of seconds to sleep before completing (runs concurrently) (default: 0)
   
   All resource consuming operations run concurrently with one another and the program exits when all are finished.

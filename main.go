package main

import (
	"flag"
	"fmt"
	"math/big"
	"runtime"
	"sync"
	"time"
)

var memoryMB int
var nthPrime int
var sleepSeconds int

func init() {
	flag.IntVar(&memoryMB, "bloat-mb", 1024, "Memory usage in MB")
	flag.IntVar(&nthPrime, "nth-prime", 1000000, "Nth prime number to calculate")
	flag.IntVar(&sleepSeconds, "sleep", 0, "Number of seconds to sleep before starting")
}

func main() {
	flag.Parse()

	done := make(chan struct{})
	var wg sync.WaitGroup

	// Sleep for the specified number of seconds
	wg.Add(1)
	go func() {
		time.Sleep(time.Duration(sleepSeconds) * time.Second)
		wg.Done()
	}()

	runtime.GOMAXPROCS(runtime.NumCPU())

	// Allocate memory
	mem := make([]byte, memoryMB*1024*1024)
	wg.Add(1)
	go func() {
		for i := range mem {
			mem[i] = byte(i % 256)
		}
		wg.Done()
	}()

	// Calculate prime numbers concurrently
	wg.Add(1)
	go func() {
		primeCounter := 0
		i := big.NewInt(2)
		for {
			if i.ProbablyPrime(20) {
				primeCounter++
				if primeCounter == nthPrime {
					break
				}
			}
			i.Add(i, big.NewInt(1))
		}
		wg.Done()
	}()

	go func() {
		wg.Wait()
		close(done)
	}()

	// Monitor progress
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-done:
			fmt.Printf("\n%dth prime number: %s\n", nthPrime, big.NewInt(0).SetBytes(mem[:8]))
			return
		case <-ticker.C:
			fmt.Print(".")
		}
	}
}
